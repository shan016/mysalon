<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = $shopinfo->name_of_salon;
//$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .border-custom {border-bottom: 1px solid #ddd;border-top: 1px solid #ddd;padding: 15px 0px;}
</style>

<div class="col-lg-7 col-md-7 col-xl-8">
    <div class="card">
        <div class="card-body">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="https://www.w3schools.com/bootstrap4/la.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="https://www.w3schools.com/bootstrap4/chicago.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="https://www.w3schools.com/bootstrap4/ny.jpg" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-5 col-md-5 col-xl-4 theiaStickySidebar">
    <div class="card">
        <div class="card-body">
            <div class="widget about-widget">
                <h5 class="widget-title">Location</h5>
                <h6 class="clinic-direction"> <i class="fas fa-map-marker-alt"></i> <?= $shopinfo->address ?>, <?= $shopinfo->city ?>, <?= $shopinfo->postcode ?>, <?= $shopinfo->district ?><br><a href="javascript:void(0);">Get Directions</a></h6>
            </div>

            <div class="widget about-widget">
                <h5 class="widget-title">About Me</h5>
                <p class="text-muted text-justify"><?= Html::encode($shopinfo->description) ?></p>
            </div>

            <div class="widget about-widget">
                <h5 class="widget-title">Contact &amp; Business hours</h5>
                <div class="patient-info">
                    <ul>
                        <li class="border-custom"><i class="fa fa-mobile text-muted fa-2x"></i> <label style="vertical-align: middle;padding-left: 10px;"><?= Html::encode($shopinfo->contact_no) ?></label> <span><a href="" class="btn btn-white call-btn"><i class="fas fa-phone"></i></a></span></li>
                    </ul>
                </div>
                <div class="patient-info">
                    <div class="listing-hours">
                        
                        <?php
                        $mytime = '';
                        $days = array(1 => 'Monday',2 => 'Tuesday',3 => 'Wednesday',4 => 'Thursday',5 => 'Friday',6 => 'Saturday',7 => 'Sunday');
                        $date = date('d-m-Y');
                        $nameOfDay = date('N', strtotime($date));
                        $shophouropen = \common\models\ShopHours::find()  
                        ->where(['shop_id' => $shopid, 'day_of_week' => $nameOfDay])
                        ->one();
                        if(!empty($shophouropen->open_time) && !empty($shophouropen->close_time)) {
                            $open = strtotime($shophouropen->open_time);
                            $close = strtotime($shophouropen->close_time);
                            $now = strtotime("now");
                            if ($now >= $open && $now <= $close) {
                                $titleo = '<span class="badge bg-success-light">Open Now</span>';
                                $mytime = '<span class="time">'.date("h:i A", strtotime($shophouropen->open_time)).' - '.date("h:i A", strtotime($shophouropen->close_time)).'</span>';
                            } else {
                                $titleo = '<span class="badge bg-danger-light">Closed</span>';
                            }
                        }else {
                            $titleo = '<span class="badge bg-danger-light">Closed</span>';
                        }

                        echo '<div class="listing-day current">
                            <div class="day">Today <span>'.date("d M Y", strtotime($date)).'</span></div>
                            <div class="time-items">
                                <span class="open-status">'.$titleo.'</span>
                                '.$mytime.'
                            </div>
                        </div>';
                        
                        foreach($shophours as $shophour) {
                            if(!empty($shophour->open_time) && !empty($shophour->close_time)) {
                                echo '<div class="listing-day">
                                    <div class="day">'.$days[$shophour->day_of_week].'</div>
                                    <div class="time-items">
                                        <span class="time">'.date("h:i A", strtotime($shophour->open_time)).' - '.date("h:i A", strtotime($shophour->close_time)).'</span>
                                    </div>
                                </div>';
                            }else {
                                echo '<div class="listing-day closed">
                                    <div class="day">'.$days[$shophour->day_of_week].'</div>
                                    <div class="time-items">
                                        <span class="time"><span class="badge bg-danger-light">Closed</span></span>
                                    </div>
                                </div>';
                            }
                        }
                        ?>
                        
                        
                        
                    </div>
                </div>

            </div>

            <div class="widget about-widget mt-5">
                <h5 class="widget-title">Social Media</h5>

                <p class="text-muted text-center">
                    <a href="javascript:void(0)" class="btn btn-white call-btn">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn btn-white fav-btn">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn btn-white fav-btn">
                        <i class="fa fa-globe"></i>
                    </a>
                </p>
            </div>

            <div class="widget about-widget mt-5">

                <a data-toggle="modal" href="#edit_time_slot">
                    <div class="listing-day">
                        <div class="text-muted">Report</div>
                        <div class="time-items">
                            <span class="open-status"><i class="fa fa-angle-right"></i></span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

</div>


