<?php

namespace app\modules\shop\controllers;

use Yii;
use common\models\Shops;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientsController implements the CRUD actions for Client model.
 */
class SalonController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex($id)
    {
        $shopinfo = \common\models\Shops::find()  
                    ->where(['shops_id' => $id])
                    ->one();
        $shophours = \common\models\ShopHours::find()  
                    ->where(['shop_id' => $id])
                    ->all();
        return $this->render('index', [
            'shopinfo' => $shopinfo,
            'shophours' => $shophours,
            'shopid' => $id,
        ]);
    }
}
