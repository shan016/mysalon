<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

?>
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <?php
                        echo Breadcrumbs::widget([
                            'tag' => 'ol',
                            'homeLink' => ['label' => 'Home', 'url' => ['/index']],
                            'activeItemTemplate' => "<li class='breadcrumb-item active'>{link}</li>",
                            'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>",
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            //'links' => '<li class="breadcrumb-item"><a href="{url}">{label}</a></li>',
                            'encodeLabels' => false,
                            //'delimiter'=>' / ',
                            'options' => array('class' => 'breadcrumb'),
                        ]);
                        //<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i></li>
                    ?>
                </nav>
                <h2 class="breadcrumb-title"><?= $this->title ?></h2>
            </div>
        </div>
    </div>
</div>

<!--<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Invoice View</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">Invoice View</h2>
            </div>
        </div>
    </div>
</div>-->
