<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
?>

<footer class="footer">

    <!-- Footer Top -->
    <div class="footer-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-about">
                        <div class="footer-logo">
                            <img src="/themes/saloon_theme/assets/img/logo.png" alt="logo">
                        </div>
                        <div class="footer-about-content">
                            <p>+1 315 369 5943</p>
                            <div class="social-icon">
                                <ul>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-dribbble"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Footer Widget -->

                </div>

                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">About Salonsly</h2>
                        <ul>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i> Careers at Salonsly</a></li>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i> Customer support</a></li>
                        </ul>
                    </div>
                    <!-- /Footer Widget -->

                </div>

                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">For business</h2>
                        
                        <ul>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i> For Partners</a></li>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i> Pricing</a></li>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i> Support for partners</a></li>
                        </ul>
                    </div>
                    <!-- /Footer Widget -->

                </div>
                
                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">Legal</h2>
                        
                        <ul>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i> Booking terms</a></li>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i> Privacy policy</a></li>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i> Website terms</a></li>
                        </ul>
                    </div>
                    <!-- /Footer Widget -->

                </div>

                <!--
                <div class="col-lg-3 col-md-6">

                     Footer Widget 
                    <div class="footer-widget footer-contact">
                        <h2 class="footer-title">Contact Us</h2>
                        <div class="footer-contact-info">
                            <div class="footer-address">
                                <span><i class="fas fa-map-marker-alt"></i></span>
                                <p> 3556  Beech Street, San Francisco,<br> California, CA 94108 </p>
                            </div>
                            <p>
                                <i class="fas fa-phone-alt"></i>
                                +1 315 369 5943
                            </p>
                            <p class="mb-0">
                                <i class="fas fa-envelope"></i>
                                doccure@example.com
                            </p>
                        </div>
                    </div>
                    <!-- /Footer Widget 

                </div> -->

            </div>
        </div>
    </div>
    <!-- /Footer Top -->

    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container-fluid">

            <!-- Copyright -->
            <div class="copyright">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="copyright-text">
                            <p class="mb-0"><a href="codeworklab.com" target="_blank">Code Work Lab</a></p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">

                        <!-- Copyright Menu -->
                        <div class="copyright-text text-right">
                            <p class="mb-0">© 2020 codeworklab.com Pvt Ltd</p>
                        </div>
                        <!-- /Copyright Menu -->

                    </div>
                </div>
            </div>
            <!-- /Copyright -->

        </div>
    </div>
    <!-- /Footer Bottom -->

</footer>