<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>

<section class="section section-doctor">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="doctor-slider slider">

                    <!-- Doctor Widget -->
                    <?php
                    foreach($models as $key=>$value) {
                        $image = common\models\Uploads::find()->where(['ref'=>$value->shops_id, 'gallery_type' => 'S'])->one();
                        $count = count($image);
                        if($count > 0) {
                           $filePath = 'http://admin.codeworklab.com'.'/upload/workplace-photos/'.$image->ref.'/thumbnail/'.$image->real_filename; 
                        }else {
                            $filePath = 'http://admin.codeworklab.com'.'/upload/workplace-photos/default.jpg';
                        }
                        
                        echo '<div class="profile-widget">
                        <div class="doc-img">
                            <a href="doctor-profile.html">
                                <img class="img-fluid" alt="User Image" src="'.$filePath.'">
                            </a>
                            <a href="javascript:void(0)" class="fav-btn">
                                <i class="far fa-bookmark"></i>
                            </a>
                        </div>
                        <div class="pro-content">
                            <h3 class="title">
                                <a href="doctor-profile.html">'.$value->name_of_salon.'</a> 
                                <i class="fas fa-check-circle verified"></i>
                            </h3>
                            
                            <div class="rating">
                                <i class="fas fa-star filled"></i>
                                <span class="d-inline-block average-rating"><b>4.7 Great</b></span>
                                <span class="d-inline-block average-rating">76 ratings</span>
                            </div>
                            <ul class="available-info">
                                <li>'.$value->address.'
                                </li>
                            </ul>
                            
                            <div class="row row-sm">
                                
                                <div class="col-12">
                                    <a href="'.Url::to(['/shop/salon/index', 'id' => $value->shops_id]).'" class="btn book-btn">Book Now</a>
                                </div>
                            </div>
                        </div>
                    </div>';
                    }
                    ?>
                    
                    <!-- /Doctor Widget -->

                </div>
            </div>
        </div>
    </div>
</section>