<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class Recommended extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $id = $session['shopID'];
        
        $models = \common\models\Shops::find()->where(['profile_public' => 'Y'])->all();

        
        return $this->render('recommended',
            ['models' => $models]
        );
        
    }
}