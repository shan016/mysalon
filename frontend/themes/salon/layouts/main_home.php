<?php
use frontend\assets\pinlist\MainAsset;

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\components\Meta;
use app\components\Header;
use app\components\Footer;
use lo\modules\noty\Wrapper;
//use common\widgets\Alert;
use yii\bootstrap\Modal;
$baseURL = Yii::$app->request->url;

MainAsset::register($this);
//AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Meta::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body >
<?php $this->beginBody() ?>
    <!-- Main Wrapper -->
    <div class="main-wrapper">

        <!-- Header -->
        <?= Header::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        <!-- /Header -->
        
        
        <!-- Page Content -->
        <?= $content ?> 
        <!-- /Page Content -->

        <!-- Footer -->
        <?= app\components\Footer::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        <!-- /Footer -->

    </div>
	   <!-- /Main Wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
