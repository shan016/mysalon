<?php
use yii\helpers\Html;
use app\components\HomeBanner;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Salonsly - Instantly book salons and spas nearby';
$this->params['breadcrumbs'][] = '<i class="fa fa-angle-right"></i>';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Home Banner -->
<?= HomeBanner::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
<!-- /Home Banner -->

<!-- Recommended -->
<?= app\components\Recommended::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
<!-- Recommended -->

<section class="section section-features">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-2 col-lg-2 d-flex">
                <div class="card flex-fill">
                    <img alt="Card Image" src="/themes/saloon_theme/assets/img/img-01.jpg" class="card-img-top">
                    <h5 class="card-title mb-0 p-3">Barbershop</h5>
                </div>
            </div>
            <div class="col-12 col-md-2 col-lg-2 d-flex">
                <div class="card flex-fill">
                    <img alt="Card Image" src="/themes/saloon_theme/assets/img/img-02.jpg" class="card-img-top">
                    <h5 class="card-title mb-0 p-3">Nail Salon</h5>
                </div>
            </div>
            <div class="col-12 col-md-2 col-lg-2 d-flex">
                <div class="card flex-fill">
                    <img alt="Card Image" src="/themes/saloon_theme/assets/img/img-03.jpg" class="card-img-top">
                    <h5 class="card-title mb-0 p-3">Hair Salon</h5>
                </div>
            </div>
            <div class="col-12 col-md-2 col-lg-2 d-flex">
                <div class="card flex-fill">
                    <img alt="Card Image" src="/themes/saloon_theme/assets/img/img-04.jpg" class="card-img-top">
                    <h5 class="card-text p-3">Eyebrows & Lashes</h5>
                </div>
            </div>
            <div class="col-12 col-md-2 col-lg-2 d-flex">
                <div class="card flex-fill">
                    <img alt="Card Image" src="/themes/saloon_theme/assets/img/img-05.jpg" class="card-img-top">
                    <h5 class="card-title mb-0 p-3">Massage</h5>
                </div>
            </div>
            <div class="col-12 col-md-2 col-lg-2 d-flex">
                <div class="card flex-fill">
                    <img alt="Card Image" src="/themes/saloon_theme/assets/img/img-06.jpg" class="card-img-top">

                    <h5 class="card-title mb-0 p-3">Beauty Salon</h5>

                </div>
            </div>
        </div>
    </div>
    
    <!-- Availabe Features -->
    <section class="section section-doctor">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5 features-img">
                    <img src="/themes/saloon_theme/assets/img/features/feature.png" class="img-fluid" alt="Feature">
                </div>
                <div class="col-md-7">
                    <div class="section-header">	
                        <h2 class="mt-2">Top categories in Colombo</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                    </div>	
                    <div class="features-slider slider">
                        <!-- Slider Item -->
                        <div class="feature-item text-center">
                            <img src="/themes/saloon_theme/assets/img/features/feature-01.jpg" class="img-fluid" alt="Feature">
                            <p>Patient Ward</p>
                        </div>
                        <!-- /Slider Item -->

                        <!-- Slider Item -->
                        <div class="feature-item text-center">
                            <img src="/themes/saloon_theme/assets/img/features/feature-02.jpg" class="img-fluid" alt="Feature">
                            <p>Test Room</p>
                        </div>
                        <!-- /Slider Item -->

                        <!-- Slider Item -->
                        <div class="feature-item text-center">
                            <img src="/themes/saloon_theme/assets/img/features/feature-03.jpg" class="img-fluid" alt="Feature">
                            <p>ICU</p>
                        </div>
                        <!-- /Slider Item -->

                        <!-- Slider Item -->
                        <div class="feature-item text-center">
                            <img src="/themes/saloon_theme/assets/img/features/feature-04.jpg" class="img-fluid" alt="Feature">
                            <p>Laboratory</p>
                        </div>
                        <!-- /Slider Item -->

                        <!-- Slider Item -->
                        <div class="feature-item text-center">
                            <img src="/themes/saloon_theme/assets/img/features/feature-05.jpg" class="img-fluid" alt="Feature">
                            <p>Operation</p>
                        </div>
                        <!-- /Slider Item -->

                        <!-- Slider Item -->
                        <div class="feature-item text-center">
                            <img src="/themes/saloon_theme/assets/img/features/feature-06.jpg" class="img-fluid" alt="Feature">
                            <p>Medical</p>
                        </div>
                        <!-- /Slider Item -->
                    </div>
                </div>
            </div>
        </div>
    </section>		
        <!-- Availabe Features -->

</section>