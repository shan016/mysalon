<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login new';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-header">
    <h3 class="card-title">Login to your Account</h3>
</div>
<div class="card-body">
    <?php $form = ActiveForm::begin(['id' => 'sign_in','options' => [
                'class' => 'login100-form validate-form'
             ]]); ?>
            <?= $form->field($model, 'email', ['options' => [],
                //'template' => "<div class=\"wrap-input100 validate-input\" data-validate=\"Enter username\">{input}<span class=\"focus-input100\" data-placeholder=\"&#xf207;\"></span>\n{error}</div>"
            ])->textInput(['placeholder' => 'Email']);
            ?>
    
            <?= $form->field($model, 'password', ['options' => [],
                //'template' => "<div class=\"wrap-input100 validate-input\" data-validate=\"Enter password\">{input}<span class=\"focus-input100\" data-placeholder=\"&#xf191;\"></span></div>{error}"
            ])->passwordInput(['placeholder' => 'Password']);
            ?>
    
            
            <div class="form-group">
                <?= $form->field($model, 'rememberMe', [
                    'template' => "<label class=\"custom-control custom-checkbox\"><a href=\"forgot-password.html\" class=\"float-right small text-dark mt-1\">I forgot password</a>{input}<span class=\"custom-control-label text-dark\">Remember me</span>\n{error}</label>"
                ])->checkbox(['class' => 'custom-control-input'],false)
                ?>
            </div>

        <div class="form-footer mt-2">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>

            


