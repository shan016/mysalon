<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets\pinlist;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //!-- Bootstrap CSS -->
	'/themes/saloon_theme/assets/css/bootstrap.min.css',
	//!-- Fontawesome CSS -->
	'/themes/saloon_theme/assets/plugins/fontawesome/css/fontawesome.min.css',
	'/themes/saloon_theme/assets/plugins/fontawesome/css/all.min.css',
        
        '/themes/saloon_theme/assets/plugins/select2/css/select2.min.css',
	'/themes/saloon_theme/assets/css/style.css',

    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];    
    public $js = [
        //<!-- Dashboard Core -->
        'themes/pinlist/assets/js/vendors/jquery-3.2.1.min.js',
        //!-- jQuery -->
	'/themes/saloon_theme/assets/js/jquery.min.js',
		
	//!-- Bootstrap Core JS -->
	'/themes/saloon_theme/assets/js/popper.min.js',
	//'/themes/saloon_theme/assets/js/bootstrap.min.js',
        '/themes/saloon_theme/assets/js/slick.js',
        
        //<!-- Sticky Sidebar JS -->
        //'/themes/saloon_theme/assets/plugins/theia-sticky-sidebar/ResizeSensor.js',
        //'/themes/saloon_theme/assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js',
		
	//<!-- Select2 JS -->
	//'/themes/saloon_theme/assets/plugins/select2/js/select2.min.js',
		
	//<!-- Custom JS -->
	'/themes/saloon_theme/assets/js/script.js',
		
	//!-- Slick JS -->
	//'/themes/saloon_theme/assets/js/slick.js',
		
	//!-- Custom JS -->
	//'/themes/saloon_theme/assets/js/script.js'
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
