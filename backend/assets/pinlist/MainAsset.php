<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets\pinlist;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'themes/pinlist/assets/fonts/fonts/font-awesome.min.css',

        //<!-- Sidemenu Css -->
        'themes/pinlist/assets/plugins/toggle-sidebar/sidemenu.css',

        //<!-- Bootstrap Css -->
        'themes/pinlist/assets/plugins/bootstrap-4.1.3/css/bootstrap.min.css',

        //<!-- Dashboard Css -->
        'themes/pinlist/assets/css/dashboard.css',
        'themes/pinlist/assets/css/admin-custom.css',
        
        //<!-- Notifications  Css -->
	'themes/pinlist/assets/plugins/notify/css/jquery.growl.css',

        //<!-- Custom scroll bar css-->
        'themes/pinlist/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css',

        //<!---Font icons-->
        'themes/pinlist/assets/plugins/iconfonts/plugin.css',
    ];
    /*public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];*/    
    public $js = [
        //<!-- Dashboard Core -->
        //'themes/pinlist/assets/js/vendors/jquery-3.2.1.min.js',
        'themes/pinlist/assets/plugins/bootstrap-4.1.3/popper.min.js',
        'themes/pinlist/assets/plugins/bootstrap-4.1.3/js/bootstrap.min.js',
        'themes/pinlist/assets/js/vendors/jquery.sparkline.min.js',
        'themes/pinlist/assets/js/vendors/selectize.min.js',
        /*'themes/pinlist/assets/js/vendors/jquery.tablesorter.min.js',
        'themes/pinlist/assets/js/vendors/circle-progress.min.js',
        'themes/pinlist/assets/plugins/rating/jquery.rating-stars.js',*/

        //<!-- Fullside-menu Js-->
        'themes/pinlist/assets/plugins/toggle-sidebar/sidemenu.js',


        //<!-- Input Mask Plugin -->
        'themes/pinlist/assets/plugins/input-mask/jquery.mask.min.js',

        //<!-- popover js -->
	//'themes/pinlist/assets/js/popover.js',

	//<!-- Notifications js -->
	//'themes/pinlist/assets/plugins/notify/js/rainbow.js',
	//'themes/pinlist/assets/plugins/notify/js/sample.js',
	'themes/pinlist/assets/plugins/notify/js/jquery.growl.js',

        //<!-- Custom scroll bar Js-->
        'themes/pinlist/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js',

        //<!-- Custom Js-->
        'themes/pinlist/assets/js/admin-custom.js',

        'themes/pinlist/assets/js/index3.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
