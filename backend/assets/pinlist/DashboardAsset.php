<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets\pinlist;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'themes/pinlist/assets/fonts/fonts/font-awesome.min.css',

        //<!-- Sidemenu Css -->
        'themes/pinlist/assets/plugins/toggle-sidebar/sidemenu.css',

        //<!-- Bootstrap Css -->
        'themes/pinlist/assets/plugins/bootstrap-4.1.3/css/bootstrap.min.css',

        //<!-- Dashboard Css -->
        'themes/pinlist/assets/css/dashboard.css',
        'themes/pinlist/assets/css/admin-custom.css',

        //<!-- JQVMap -->
        'themes/pinlist/assets/plugins/jqvmap/jqvmap.min.css',

        //<!-- Morris.js Charts Plugin -->
        'themes/pinlist/assets/plugins/morris/morris.css',

        //<!-- Custom scroll bar css-->
        'themes/pinlist/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css',

        //<!---Font icons-->
        'themes/pinlist/assets/plugins/iconfonts/plugin.css',
    ];
    /*public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];*/    
    public $js = [
        //<!-- Dashboard Core -->
        'themes/pinlist/assets/js/vendors/jquery-3.2.1.min.js',
        'themes/pinlist/assets/plugins/bootstrap-4.1.3/popper.min.js',
        'themes/pinlist/assets/plugins/bootstrap-4.1.3/js/bootstrap.min.js',
        'themes/pinlist/assets/js/vendors/jquery.sparkline.min.js',
        'themes/pinlist/assets/js/vendors/selectize.min.js',
        'themes/pinlist/assets/js/vendors/jquery.tablesorter.min.js',
        'themes/pinlist/assets/js/vendors/circle-progress.min.js',
        'themes/pinlist/assets/plugins/rating/jquery.rating-stars.js',

        //<!-- Fullside-menu Js-->
        'themes/pinlist/assets/plugins/toggle-sidebar/sidemenu.js',


        //<!-- Input Mask Plugin -->
        'themes/pinlist/assets/plugins/input-mask/jquery.mask.min.js',

        //<!-- JQVMap -->
        'themes/pinlist/assets/plugins/jqvmap/jquery.vmap.js',
        'themes/pinlist/assets/plugins/jqvmap/maps/jquery.vmap.world.js',
        'themes/pinlist/assets/plugins/jqvmap/jquery.vmap.sampledata.js',

        //<!-- ECharts Plugin -->
        'themes/pinlist/assets/plugins/echarts/echarts.js',

        //<!-- jQuery Sparklines -->
        'themes/pinlist/assets/plugins/jquery-sparkline/jquery.sparkline.min.js',

        //<!-- Flot Chart -->
        'themes/pinlist/assets/plugins/flot/jquery.flot.js',
        'themes/pinlist/assets/plugins/flot/jquery.flot.fillbetween.js',
        'themes/pinlist/assets/plugins/flot/jquery.flot.pie.js',

        //<!--Counters -->
        'themes/pinlist/assets/plugins/counters/counterup.min.js',
        'themes/pinlist/assets/plugins/counters/waypoints.min.js',

        //<!-- Custom scroll bar Js-->
        'themes/pinlist/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js',

        //<!-- Custom Js-->
        'themes/pinlist/assets/js/admin-custom.js',

        'themes/pinlist/assets/js/index3.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
	'yii\bootstrap\BootstrapPluginAsset',
    ];
}
