<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets\pinlist;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'themes/pinlist/assets/fonts/fonts/font-awesome.min.css',
        
	// Bootstrap Css -->
	'themes/pinlist/assets/plugins/bootstrap-4.1.3/css/bootstrap.css',
        //'//stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.css',

	// Sidemenu Css -->
	'themes/pinlist/assets/plugins/toggle-sidebar/sidemenu.css',

	// Dashboard css -->
	'themes/pinlist/assets/css/dashboard.css',
	'themes/pinlist/assets/css/admin-custom.css',

	// c3.js Charts Plugin -->
	'themes/pinlist/assets/plugins/charts-c3/c3-chart.css',

	//-Font icons-->
	'themes/pinlist/assets/plugins/iconfonts/plugin.css',
    ];
    /*public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];*/    
    public $js = [
        // Dashboard js -->
        'themes/pinlist/assets/js/vendors/jquery-3.2.1.min.js',
        'themes/pinlist/assets/plugins/bootstrap-4.1.3/popper.min.js',
        'themes/pinlist/assets/plugins/bootstrap-4.1.3/js/bootstrap.min.js',
        /*'themes/pinlist/assets/js/vendors/jquery.sparkline.min.js',
        'themes/pinlist/assets/js/vendors/selectize.min.js',
        'themes/pinlist/assets/js/vendors/jquery.tablesorter.min.js',
        'themes/pinlist/assets/js/vendors/circle-progress.min.js',
        'themes/pinlist/assets/plugins/rating/jquery.rating-stars.js',*/
        // Custom scroll bar Js-->
        'themes/pinlist/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js',

        // Fullside-menu Js-->
        //'themes/pinlist/assets/plugins/toggle-sidebar/sidemenu.js',


        //Counters -->
        //'themes/pinlist/assets/plugins/counters/counterup.min.js',
        //'themes/pinlist/assets/plugins/counters/waypoints.min.js',

        // Custom Js-->
        'themes/pinlist/assets/js/admin-custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
