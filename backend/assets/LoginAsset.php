<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //<!-- Google Fonts -->
        'https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext',
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        //<!-- Bootstrap Core Css -->
        //'themes/gentelella-master/login/css/bootstrap.css',
        'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700',
	//<!-- icons -->
        'themes/saloon_theme/plugins/font-awesome/css/font-awesome.min.css',
	'themes/saloon_theme/plugins/iconic/css/material-design-iconic-font.min.css',
        //<!-- bootstrap -->
	'themes/saloon_theme/plugins/bootstrap/css/bootstrap.min.css',
        //<!-- style -->
        'themes/saloon_theme/css/pages/extra_pages.css'


    ];
    /*public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];*/    
    public $js = [
        //'themes/saloon_theme/gentelella-master/login/js/jquery.min.js',
        //<!-- start js include path -->
        'themes/saloon_theme/plugins/jquery/jquery.min.js',
        //<!-- bootstrap -->
        'themes/saloon_theme/plugins/bootstrap/js/bootstrap.min.js',
        'themes/saloon_theme/js/pages/extra_pages/login.js'
        //<!-- end js include path -->
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
