<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        //'themes/saloon_theme/plugins/font-awesome/css/font-awesome.min.css',
        //google font
        '//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700',
	//icons
        'themes/saloon_theme/plugins/simple-line-icons/simple-line-icons.min.css',
        'themes/saloon_theme/plugins/font-awesome/css/font-awesome.min.css',
	//bootstrap
        'themes/saloon_theme/plugins/bootstrap/css/bootstrap.min.css',
	'themes/saloon_theme/plugins/summernote/summernote.css',
	//morris chart
        'themes/saloon_theme/plugins/morris/morris.css',
        //Material Design Lite CSS
	'themes/saloon_theme/plugins/material/material.min.css',
	'themes/saloon_theme/css/material_style.css',
	//animation
	'themes/saloon_theme/css/pages/animate_page.css',
	//Template Styles
        'themes/saloon_theme/css/plugins.min.css',
        'themes/saloon_theme/css/style.css',
        'themes/saloon_theme/css/responsive.css',
        'themes/saloon_theme/css/theme-color.css',
	
    ];
    public $js = [
        //start js include path
        'themes/saloon_theme/plugins/jquery/jquery.min.js',
        'themes/saloon_theme/plugins/popper/popper.min.js',
        'themes/saloon_theme/plugins/jquery-blockui/jquery.blockui.min.js',
        'themes/saloon_theme/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        //bootstrap
        'themes/saloon_theme/plugins/bootstrap/js/bootstrap.min.js',
        'themes/saloon_theme/plugins/sparkline/jquery.sparkline.min.js',
        'themes/saloon_theme/js/pages/sparkline/sparkline-data.js',
        //Common js-->
        'themes/saloon_theme/js/app.js',
        'themes/saloon_theme/js/layout.js',
        'themes/saloon_theme/js/theme-color.js',
        //material
        'themes/saloon_theme/plugins/material/material.min.js',
        //animation
        'themes/saloon_theme/js/pages/ui/animations.js',
        //morris chart
        'themes/saloon_theme/plugins/morris/morris.min.js',
        'themes/saloon_theme/plugins/morris/raphael-min.js',
        'themes/saloon_theme/js/pages/chart/morris/morris_home_data.js',
        //end js include path
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
