<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>

<footer class="footer">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
                Copyright &copy; 2019. Designed by <a href="#">Code Work Lab</a> All rights reserved.
            </div>
        </div>
    </div>
</footer>