<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
$session = Yii::$app->session;
$clientID = $session['currentclientID'];
$shopID = $session['shopID'];

?>

<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar doc-sidebar">
    <div class="app-sidebar__user clearfix">
        <div class="dropdown user-pro-body">
            <div>
                <?php echo Html::img('@web/upload/profile/user2-160x160.jpg'. '', ['alt' => 'some', 'class' => 'avatar avatar-lg brround']); ?>
                <a href="editprofile.html" class="profile-img">
                    <span class="fa fa-pencil" aria-hidden="true"></span>
                </a>
            </div>
            <div class="user-info">
                <h2><?= Yii::$app->user->identity->profile->profile_name ?></h2>
                <span>Online</span>
            </div>
        </div>
    </div>
    
    <?php
        $myurl = Yii::$app->request->url;
        $menuItems[] = '';
        $adminmenu = '';
        $shop_menu = '';
        $home = ['label' => '<i class="side-menu__icon fa fa-home"></i> <span class="side-menu__label">Dashboard</span></i>', 
            'url' => ['/site'],
            'template' => '<a class="side-menu__item" href="#">{label}</a>',
            //'options' => ['class' => 'side-menu__item']
        ];
        
        if(Yii::$app->user->identity->user_type == 'A') {
            $adminmenu = ['label' => '<i class="side-menu__icon fa fa-cogs"></i> <span class="side-menu__label">Manage Clients</span>',
                'url' => ['#'],
                'template' => '<a class="side-menu__item" data-toggle="slide" href="#">{label}<i class="angle fa fa-angle-right"></i></a>',
                'options' => ['class' => 'slide'],
                //'linkOptions'=>['class'=>'slide-item'],
                'items' => [
                    ['label' => 'Manage Clients', 'url' => ['/admin/clients/index'], 'active' => $myurl == '/admin/clients/index'],
                    ['label' => 'Admin Client', 'url' => ['/admin/clients/create'], 'active' => $myurl == '/admin/clients/create'],
                    
                    //['label' => 'Logo', 'url' => ['/settings/logo'], 'active' => $myurl == '/settings/logo'],
                    //['label' => 'Email Template', 'url' => ['/admin/email-template'], 'active' => $myurl == '/admin/email-template'],
                    //['label' => 'SMS Template', 'url' => ['/admin/sms-template'], 'active' => $myurl == '/admin/sms-template'],
                ],
            ];
        }
        
        if(!empty($shopID)) {
            $shop_menu = ['label' => '<i class="side-menu__icon fa fa-bank"></i> <span class="side-menu__label">Business Profile</span>',
                'url' => ['#'],
                'template' => '<a class="side-menu__item" data-toggle="slide" href="#">{label}<i class="angle fa fa-angle-right"></i></a>',
                'options' => ['class' => 'slide'],
                //'linkOptions'=>['class'=>'slide-item'],
                'items' => [
                    ['label' => 'Salon Info', 'url' => ['/business/salon/index'], 'active' => $myurl == '/business/salon/index'],
                    ['label' => 'Categories', 'url' => ['/business/categories/index'], 'active' => $myurl == '/business/categories/index'],
                    ['label' => 'Workplace Photos', 'url' => ['/business/workplace-photos/index'], 'active' => $myurl == '/business/workplace-photos/index'],
                    ['label' => 'Opening Hours', 'url' => ['/business/opening-hours/index'], 'active' => $myurl == '/business/opening-hours/index'],
                    ['label' => 'Staff Members', 'url' => ['/business/staff/index'], 'active' => $myurl == '/business/staff/index'],
                    ['label' => 'Services', 'url' => ['/business/service/index'], 'active' => $myurl == '/business/service/index'],
                    ['label' => 'See Our Work', 'url' => ['/business/see-our-work/index'], 'active' => $myurl == '/business/see-our-work/index'],
                ],
            ];
        }
        

        $menuItems = [
            $home,
            $adminmenu,
            $shop_menu,
        ];
?>
    
<?php
    echo Menu::widget([
        'options' => ['class' => 'side-menu'],
        'items' => $menuItems,        
        'submenuTemplate' => "\n<ul class='slide-menu'>\n{items}\n</ul>\n",
        'linkTemplate' => '<a class="slide-item" href="{url}">{label}</a>',
        'encodeLabels' => false, //allows you to use html in labels
        'activateParents' => true,]);
?>    

   
    <div class="app-sidebar-footer">
        <a href="emailservices.html">
            <span class="fa fa-envelope" aria-hidden="true"></span>
        </a>
        <a href="profile.html">
            <span class="fa fa-user" aria-hidden="true"></span>
        </a>
        <a href="editprofile.html">
            <span class="fa fa-cog" aria-hidden="true"></span>
        </a>
        <a href="login.html">
            <span class="fa fa-sign-in" aria-hidden="true"></span>
        </a>
        <a href="chat.html">
            <span class="fa fa-comment" aria-hidden="true"></span>
        </a>
    </div>
</aside>