<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class SidebarMenu extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        //$session = Yii::$app->session;
        //$clientID = $session['currentclientID'];

        
        return $this->render('sidebar_menu',
            []
        );
        
    }
}