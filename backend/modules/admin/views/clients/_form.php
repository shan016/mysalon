<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-row">
        <?=
        $form->field($model, 'company_name', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
    </div>

    

    
    <div class="form-row">
        <?=
        $form->field($model, 'company_email', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
        
        <?=
        $form->field($model, 'company_tel', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
        
        <?=
        $form->field($model, 'company_fax', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
        
        <?=
        $form->field($model, 'shop_link', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
    </div>

    <div class="form-row">
        <?=
        $form->field($model, 'short_business_description', [
            'options' => [
                'class' => 'form-group col-md-12',
            ]
        ])->textarea(['rows' => 6]) ?>
    
    </div>
    
    



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
