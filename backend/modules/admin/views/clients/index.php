<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12 col-lg-12">
    <div class="card">
        <div class="card-header">
            <div class="card-title"><?= Html::encode($this->title) ?></div>
            <div class="card-options">
                <?= Html::a('Add Client', ['create'], ['class' => 'btn btn-sm btn-success mr-2']) ?>
            </div>
        </div>
        <div class="card-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <div class="table-responsive client-index">
                <?= GridView::widget([
        'tableOptions' => ['class' => 'hover table-bordered border-top-0 border-bottom-0', 'id' => 'example2'],
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'company_name',
            //'name_of_salon',
            //'address',
            //'postcode',
            //'city',
            //'province',
            //'district',
            //'lat',
            //'lng',
            'company_email:email',
            'company_tel',
            'company_fax',
            //'shop_link:ntext',
            //'short_business_description:ntext',
            //'profile_public',
            'client_status',

            //['class' => 'yii\grid\ActionColumn'],
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}', //{view} {delete}
            'buttons' => [
                'update' => function ($url, $model) {
                    return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                }, /* ,
                      'view' => function ($url, $model) {
                      return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                      },
                      'delete' => function ($url, $model) {
                      return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                      } */
                    ],
                //'visible' => $visible,
            ],
        ],
    ]); ?>
            </div>
        </div>
    </div>
</div>

