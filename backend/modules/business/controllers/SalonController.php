<?php

namespace app\modules\business\controllers;

use Yii;
use common\models\Shops;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientsController implements the CRUD actions for Client model.
 */
class SalonController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $id = $session['shopID'];
        
        //Yii::$app->session->setFlash('success', 'CUSTOM TITLE | noty success');
        //\Yii::$app->getSession()->setFlash('success', 'CUSTOM TITLE | noty success');
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($model->profile_public == 1) {
                $model->profile_public = 'Y';
            }else {
                $model->profile_public = 'N';
            }
            if($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Success | Successfully saved');
            }else {
                \Yii::$app->getSession()->setFlash('error', 'Oops! | Something went wrong');
            }
            
            return $this->redirect(['index']);
            
        }
        if($model->profile_public == 'Y') {$model->profile_public = true;}
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    
    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shops::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
