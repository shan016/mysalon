<?php

namespace app\modules\business\controllers;

use Yii;
use common\models\Shops;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientsController implements the CRUD actions for Client model.
 */
class OpeningHoursController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $shopID = $session['shopID'];
        $model = new \common\models\ShopHoursForm();

        if ($model->load(Yii::$app->request->post())) {
            \common\models\ShopHours::deleteAll(['shop_id' => $shopID]);
            $data_recruiter_reward = array();
            //$value ="";
            foreach ($model->day_of_week as $index => $value) {
                $value1 = $model->open_time[$index];
                $value2 = $model->close_time[$index];
                $data_recruiter_reward[] = [$shopID,$index,$value1,$value2];
            }

            Yii::$app->db
                ->createCommand()
                ->batchInsert('shop_hours', ['shop_id','day_of_week','open_time','close_time'],$data_recruiter_reward)
                ->execute();
            //\Yii::$app->getSession()->setFlash('success',['title' => 'Recruiter Rewards', 'text' => 'Action successful!']);
            \Yii::$app->getSession()->setFlash('success', 'Success | Successfully saved');
            return $this->redirect(['index']);            
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shops::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
