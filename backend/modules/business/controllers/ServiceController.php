<?php

namespace app\modules\business\controllers;

use Yii;
use common\models\Service;
use common\models\ServiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Service model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Service model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $shopID = $session['shopID'];
        
        $model = new Service();

        if ($model->load(Yii::$app->request->post())) {
            $strtotime = $model->s_hour.':'.$model->s_minute.':00';
            $model->shop_id = $shopID;
            $model->duration = $strtotime;
            if($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Success | Successfully saved');
            }else {
                print_r($model->getErrors());die;
                \Yii::$app->getSession()->setFlash('error', 'Oops | Something went wrong');
            }
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->service_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Service model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $shopID = $session['shopID'];
        
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
            $strtotime = $model->s_hour.':'.$model->s_minute.':00';
            $model->duration = $strtotime;
            if($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Success | Successfully saved');
            }else {
                print_r($model->getErrors());die;
                \Yii::$app->getSession()->setFlash('error', 'Oops | Something went wrong');
            }
            return $this->redirect(['index']);
        }
        $hour = date('H',strtotime($model->duration));
        if($hour > 0) {
            $model->s_hour = date('g',strtotime($model->duration));
        }        
        $model->s_minute = date('i',strtotime($model->duration));
        //$strtotime = $model->s_hour.':'.$model->s_minute.':00';
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Service::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
