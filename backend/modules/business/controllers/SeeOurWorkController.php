<?php

namespace app\modules\business\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

use yii\imagine;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\ImageInterface;

//Models
use common\models\Shops;
use common\models\Uploads;

/**
 * ClientsController implements the CRUD actions for Client model.
 */
class SeeOurWorkController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/themes/pinlist/layouts/main_file_upload';
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $id = $session['shopID'];
        
        $datas = Uploads::find()->where(['ref'=>$id, 'gallery_type' => 'W'])->all();
        return $this->render('index', [
            'datas'=>$datas,
        ]);
    }
    
    public function actionImages()
    {
        $this->layout = '@app/themes/pinlist/layouts/main_file_upload';
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $id = $session['shopID'];
        
        $action=Yii::$app->request->post('action');
        if (Yii::$app->request->post()){
            $this->Uploads(false);
            \Yii::$app->getSession()->setFlash('success', 'Success | Successfully saved');
            return $this->redirect(['index']); 
        }else {
            list($initialPreview,$initialPreviewConfig) = $this->getInitialPreview($id);

            return $this->render('update', [
                //'model' => $model,
                'initialPreview'=>$initialPreview,
                    'initialPreviewConfig'=>$initialPreviewConfig,
            ]);
        }
        
        
        //Yii::$app->session->setFlash('success', 'CUSTOM TITLE | noty success');
        //\Yii::$app->getSession()->setFlash('success', 'CUSTOM TITLE | noty success');
        
        
    }
    
    //$this->Uploads(false);
    
    
    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shops::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /*|*********************************************************************************|
  |================================ Upload Ajax ====================================|
  |*********************************************************************************|*/
    public function actionUploadajax(){
           $this->Uploads(true);
     }
    private function CreateDir($folderName){
        if($folderName != NULL){
            $basePath = Yii::getAlias('@webroot').'/upload/see-our-work/';
            //$basePath = VIPProduct::getUploadPath();
            //FileHelper::createDirectory($basePath.$folderName,0777);
            if(FileHelper::createDirectory($basePath.$folderName,0777)){
                FileHelper::createDirectory($basePath.$folderName.'/thumbnail',0777);
            }
        }
        return;
    }
    private function removeUploadDir($dir){
        FileHelper::removeDirectory(VIPProduct::getUploadPath().$dir);
    }
    private function Uploads($isAjax=false) {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $ref = $session['shopID'];
        
             if (Yii::$app->request->isPost) {

                $images = UploadedFile::getInstancesByName('upload_ajax');
                if ($images) {
                    /*if($isAjax===true){
                        $ref ='llll';
                    }else{
                        $PhotoLibrary = Yii::$app->request->post('VIPProduct');
                        $ref = $PhotoLibrary['ref'];
                    }*/

                    $this->CreateDir($ref);
                    foreach ($images as $file){
                        $fileName       = $file->baseName . '.' . $file->extension;
                        $realFileName   = md5($file->baseName.time()) . '.' . $file->extension;
                        $savePath       = 'upload/see-our-work/'.$ref.'/'. $realFileName;
    
                    
                        if($file->saveAs($savePath)){
                            if($this->isImage(Url::base(true).'/'.$savePath)){
                                //var_dump(createThumbnail($ref,$realFileName));
                                $this->createThumbnail($ref,$realFileName);
                            }
                          
                            $model                  = new Uploads;
                            $model->ref             = $ref;
                            $model->gallery_type    = 'W';
                            $model->file_name       = $fileName;
                            $model->real_filename   = $realFileName;
                            $model->save();
                            if($isAjax===true){
                                echo json_encode(['success' => 'true']);
                            }
                            
                        }else{
                            if($isAjax===true){
                                echo json_encode(['success'=>'false','eror'=>$file->error]);
                            }
                        } 
                    }
                }
            }
    }
    private function getInitialPreview($ref) {
            $datas = Uploads::find()->where(['ref'=>$ref, 'gallery_type' => 'W'])->all();
            $initialPreview = [];
            $initialPreviewConfig = [];
            foreach ($datas as $key => $value) {
                array_push($initialPreview, $this->getTemplatePreview($value));
                array_push($initialPreviewConfig, [
                    'caption'=> $value->file_name,
                    'width'  => '120px',
                    'url'    => Url::to(['/business/workplace-photos/deletefile-ajax']),
                    'key'    => $value->upload_id
                ]);
            }
            return  [$initialPreview,$initialPreviewConfig];
    }
    public function isImage($filePath){
            return @is_array(getimagesize($filePath)) ? true : false;
    }
    private function getTemplatePreview(Uploads $model){     
            //$filePath = VIPProduct::getUploadUrl().$model->ref.'/thumbnail/'.$model->real_filename;
            $filePath = Url::base(true).'/upload/see-our-work/'.$model->ref.'/thumbnail/'.$model->real_filename;
            $isImage  = $this->isImage($filePath);
            if($isImage){
                $file = Html::img($filePath,['class'=>'file-preview-image', 'alt'=>$model->file_name, 'title'=>$model->file_name]);
            }else{
                $file =  "<div class='file-preview-other'> " .
                         "<h2><i class='glyphicon glyphicon-file'></i></h2>" .
                         "</div>";
            }
            return $file;
    }
    private function createThumbnail($folderName,$fileName,$width=270){
      $uploadPath   = Yii::getAlias('@webroot').'/'.'upload/see-our-work/'.$folderName.'/'; 
      $file         = $uploadPath.$fileName;
      $image        = Yii::$app->image->load($file);
      $image->resize($width);
      $image->save($uploadPath.'thumbnail/'.$fileName);
      return;
    }
    
    public function actionDeletefileAjax(){
        $this->enableCsrfValidation = false;
        $model = Uploads::findOne(Yii::$app->request->post('key'));
        
        if($model!==NULL){
            $filename  = 'upload/see-our-work/'.$model->ref.'/'.$model->real_filename;
            $thumbnail = 'upload/see-our-work/'.$model->ref.'/thumbnail/'.$model->real_filename;
            if($model->delete()){
                @unlink($filename);
                @unlink($thumbnail);
                echo json_encode(['success'=>true]);
            }else{
                print_r($model->getErrors());
                echo json_encode(['success'=>false]);
            }
        }else{
          echo json_encode(['success'=>false]);  
        }
    }
}
