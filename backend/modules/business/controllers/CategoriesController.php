<?php

namespace app\modules\business\controllers;

use Yii;
use common\models\Shops;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientsController implements the CRUD actions for Client model.
 */
class CategoriesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $shopID = $session['shopID'];
        $model = new \common\models\CategoriesForm();
        
        $mylist = "";
        $main_category = "";
        $business_categories_id = "";
        $list2 = \common\models\BusinessCategoriesSaloon::find()->where(['shops_id' => $shopID])->all();
        foreach ($list2 as $list){
            $mylist[] = $list->business_categories_id;
            //$selctdd = \common\models\BusinessCategoriesSaloon::find()->where(['business_categories_id' => $list->business_categories_id, 'shops_id' => $shopID, 'default_id' => 1])->one();
            //$business_categories_id = $selctdd->business_categories_id;
        }
        
        $selctdd = \common\models\BusinessCategoriesSaloon::find()->where(['shops_id' => $shopID, 'default_id' => 1])->count();
        if($selctdd > 0) {
            $selctdd = \common\models\BusinessCategoriesSaloon::find()->where(['shops_id' => $shopID, 'default_id' => 1])->one();
            $main_category = $selctdd->business_categories_id;
        }

        $model->category = $mylist;
        $model->main_category = $main_category;
        
        //Yii::$app->session->setFlash('success', 'CUSTOM TITLE | noty success');
        //\Yii::$app->getSession()->setFlash('success', 'CUSTOM TITLE | noty success');

        if ($model->load(Yii::$app->request->post())) {
            \common\models\BusinessCategoriesSaloon::deleteAll(['shops_id' => $shopID]);
            $data_recruiter_reward = array();
            foreach ($model->category as $selected) {
                //print_r($selected);
                //die;
                $data_recruiter_reward[] = [$selected,$shopID,0];
            }
            if(!empty($model->main_category)) {
                $data_recruiter_reward[] = [$model->main_category,$shopID,1];
            }
            Yii::$app->db
                ->createCommand()
                ->batchInsert('business_categories_saloon', ['business_categories_id','shops_id','default_id'],$data_recruiter_reward)
                ->execute();
            //\Yii::$app->getSession()->setFlash('success',['title' => 'Recruiter Rewards', 'text' => 'Action successful!']);
            \Yii::$app->getSession()->setFlash('success', 'Success | Successfully saved');
            return $this->redirect(['index']);            
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    
    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shops::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
