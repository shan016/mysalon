<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Services';
$this->params['breadcrumbs'][] = $this->title;
$services = $dataProvider->getModels();
$count = count($services);
?>
<style>
    .mybglist {    background: #faf9f7;
    border: 1px solid #e1e1e1;
    border-radius: 4px; border-left: 2px solid #ff2f00 !important;}
    .mybglist h4 {margin: 0px;}
</style>

<div class="col-lg-12">
    <div class="card service-index">
        <div class="card-header">
            <h3 class="card-title"><?= Html::encode($this->title) ?></h3>
            
        </div>
        <div class="card-body">
            <p>
            <?= Html::a('Add Service', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Add Category', ['create'], ['class' => 'btn btn-info']) ?>
            </p>
            <?php if ($count > 0) {
                foreach ($services as $index => $service) {
                    $myhour = '';
                    $hour = date('H',strtotime($service->duration));
                    $mint = date('i',strtotime($service->duration));
                    if($hour > 0) {
                        $hour = date('g',strtotime($service->duration));
                        $myhour = $hour.'h:'.$mint.'min';
                    }else {
                        $myhour = $mint.'min';
                    }
                    
                    if($service->price_type == 'free') {
                        $price = 'Free';
                    }else {
                        $price = 'LKR '.$service->price;
                    }

                    echo '<div class="alert alert-secondary mybglist">
                        <div class="row">
                            <div class="col-md-7"><h4>'.$service->service_name.'</h4></div>
                            <div class="col-md-2 text-right">'.$myhour.'</div>
                            <div class="col-md-2 text-right">'.$price.'</div>
                            <div class="col-md-1 text-right">'.Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $service->service_id]).'</div>
                        </div>
                    </div>';
                }
            } ?>
    </div>
</div>