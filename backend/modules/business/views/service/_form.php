<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $form yii\widgets\ActiveForm */

    $count = 0;
    $h_start = "0"; //you can write here 00:00:00 but not need to it
    $h_end = "23";
    while($h_start <= $h_end){
        $open_time[] = array('h_start_value' => $h_start,'h_start_label' => $h_start.' hour(s)');
        $h_start++;
    }
    $hour_list = ArrayHelper::map($open_time, 'h_start_value', 'h_start_label');
    
    $m_start = "0"; //you can write here 00:00:00 but not need to it
    $m_end = "55";
    while($m_start <= $m_end){
        $m_time[] = array('m_start_value' => $m_start,'m_start_label' => $m_start.' minutes');
        $m_start+= 5;
    }
    $minutes_list = ArrayHelper::map($m_time, 'm_start_value', 'm_start_label');

?>
<div class="service-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'service_name')->textInput(['maxlength' => true]) ?>

    <?php
    $service_categories_id = ['0' => 'Not categorized'];
    echo $form->field($model, "service_categories_id")->dropDownList($service_categories_id, [
        //'prompt' => '-- Open --',
    ]);
    ?>
    <div class="row">
        <div class="col-sm-12">
        <label class="control-label" for="service-service_categories_id">Duration</label>
        </div>
        <div class="col-sm-6">
            <?php            
            echo $form->field($model, "s_hour")->dropDownList($hour_list, [
                //'prompt' => '-- Open --',
            ])->label(false);
            ?>
        </div>
        <div class="col-sm-6">

            <?php            
            echo $form->field($model, "s_minute")->dropDownList($minutes_list, [
                //'prompt' => '-- Open --',
            ])->label(false);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
        <label class="control-label" for="service-service_categories_id">Price Type & Price</label>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'price_type')->dropDownList(['fixed' => 'Fixed', 'free' => 'Free'], [
                //'prompt' => '-- Open --',
            ])->label(false);
            ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true, 'placeholder'=> '0.00'])->label(false) ?>
        </div>
    </div>
    

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
