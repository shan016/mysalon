<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Service */

$this->title = 'Edit Service';
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->service_id, 'url' => ['view', 'id' => $model->service_id]];
//$this->params['breadcrumbs'][] = 'Update';
?>

<div class="col-xl-6 service-update">
    <div class="card m-b-20">
        <div class="card-header">
            <h3 class="card-title"><?= Html::encode($this->title) ?></h3>

        </div>
        <div class="card-body">
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>
        </div>
    </div>
</div>