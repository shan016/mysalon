<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ServiceCategories */

$this->title = 'Update Service Categories: ' . $model->service_categories_id;
$this->params['breadcrumbs'][] = ['label' => 'Service Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->service_categories_id, 'url' => ['view', 'id' => $model->service_categories_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="service-categories-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
