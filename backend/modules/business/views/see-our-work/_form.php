<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

<?=Html::beginForm(['see-our-work/images'],'post',['enctype' => 'multipart/form-data']);?>
    <div class="row form-group">
        <div class="col-lg-12">
            <?php
            echo FileInput::widget([
    'name' => 'upload_ajax[]',
    'options'=>[
        'multiple'=>true
    ],
    'pluginOptions' => [
        'showUpload' => false,
        'showRemove' => false,
        'showCancel' => false,
        'overwriteInitial' => false,
        'initialPreviewShowDelete' => true,        
        'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'bmp', 'tiff'],
        'initialPreview' => $initialPreview,
        //'initialPreview'=> $initialPreview[$model->ref],
        //'initialPreviewConfig'=>$model->initialPreview($model->docs,'docs','config'),
        'initialPreviewConfig' => $initialPreviewConfig,
        'uploadUrl' => Url::to(['/business/see-our-work/uploadajax']),
        'uploadAsync' => false,
        'maxFileCount' => 20
    ]
]);
            ?>

        </div>
        
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block','method' => 'post',]) ?>
    </div>
<?= Html::endForm();?> 
</div>
