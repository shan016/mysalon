<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-row">
        <?=
        $form->field($model, 'name_of_salon', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
    </div>
    <div class="form-row">
        <?=
        $form->field($model, 'contact_no', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
    </div>
    <div class="form-row">
        <?=
        $form->field($model, 'description', [
            'options' => [
                'class' => 'form-group col-md-12',
            ]
        ])->textarea(array('rows'=>3,'cols'=>5))
        ?>
    </div>

    <div class="form-row">
        <?=
        $form->field($model, 'address', [
            'options' => [
                'class' => 'form-group col-md-12',
            ]
        ])->textInput(['maxlength' => true])
        ?>
    </div>

    
    <div class="form-row">
        <?=
        $form->field($model, 'city', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
        
        <?=
        $form->field($model, 'postcode', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
        
        <?=
        $form->field($model, 'province', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
        
        <?=
        $form->field($model, 'district', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true])
        ?>
    </div>

    <div class="form-row">
        <?=
        $form->field($model, 'lat', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true]) ?>
        
        <?=
        $form->field($model, 'lng', [
            'options' => [
                'class' => 'form-group col-md-6',
            ]
        ])->textInput(['maxlength' => true]) ?>
    </div>
    <?=
    $form->field($model, 'main_shop', [
        'template' => "<div class='form-group'><div class='form-label'>Make Main Shop</div><label class='custom-switch'>{input}<span class='custom-switch-indicator'></span><span class='custom-switch-description'>Mark as main salon</span></label>{error}</div>"
    ])->checkbox(['class' => 'custom-switch-input'], false)
    ?>
    
    <?=
    $form->field($model, 'profile_public', [
        'template' => "<div class='form-group'><div class='form-label'>Make your profile public</div><label class='custom-switch'>{input}<span class='custom-switch-indicator'></span><span class='custom-switch-description'>Your business profile and schedule is visible for your clients so they can self-book 24/7 using the Booksy Client app or Web.</span></label>{error}</div>"
    ])->checkbox(['class' => 'custom-switch-input'], false)
    ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
