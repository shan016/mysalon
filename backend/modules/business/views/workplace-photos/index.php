<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = 'Workplace Photos';
//$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->company_name, 'url' => ['view', 'id' => $model->client_id]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="col-md-12 col-lg-12 client-update">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><?= Html::encode($this->title) ?></h3>
        </div>
        
        <div class="card-body">
            <div class="mail-option">
                               
                <div class="btn-group hidden-phone">
                    <?= Html::a('<i class="fa fa-plus"></i> Add New', ['images'], ['class'=>'btn btn-primary bg-primary text-white']); ?>
                </div>
            </div>
            
            <div class="demo-gallery">
                <?php 
                if(count($datas) > 0) {
                    echo '<ul id="lightgallery" class="list-unstyled row ">';
                    foreach ($datas as $model) {
                        $filePath = Url::base(true).'/upload/workplace-photos/'.$model->ref.'/thumbnail/'.$model->real_filename;
                        $isImage  = @is_array(getimagesize($filePath)) ? true : false;
                        if($isImage){
                            $filePathBig = Url::base(true).'/upload/workplace-photos/'.$model->ref.'/'.$model->real_filename;
                            //$file = Html::img($filePath,['class'=>'file-preview-image', 'alt'=>$model->file_name, 'title'=>$model->file_name]);
                            echo '<li class="col-xs-6 col-sm-4 col-md-3" data-responsive="'.$filePathBig.'" data-src="'.$filePathBig.'" data-sub-html="<h4>Gallery Image 1</h4>" >
                                <a href="">
                                    <img class="img-responsive" src="'.$filePath.'" alt="Thumb-1">
                                </a>
                            </li>';
                        }
                    }
                    echo '</ul>';
                }
                ?>
            </div>
        </div>
    </div>
</div>


