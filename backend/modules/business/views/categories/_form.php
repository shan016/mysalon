<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="client-form">
    
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-lg-6">
        <h3 class="card-title">Categories of Your Business</h3>
        <?php
        $count = 0;
        $item_lists = \yii\helpers\ArrayHelper::map(common\models\BusinessCategories::find()
                        ->all(), "id", "name");
        
        
        echo $form->field($model, 'category')->checkboxList($item_lists, [
                'item' =>
                function ($index, $label, $name, $checked, $value) {
                    $checked = $checked ? 'checked' : '';
                    $session = Yii::$app->session;
                    $shopID = $session['shopID'];
                    $count = common\models\BusinessCategoriesSaloon::find()->where(['business_categories_id' => $value, 'shops_id' => $shopID, 'default_id' => 1])->count();
                    $disable = false;
                    if($count > 0) {
                        $disable = true;
                    }
                    return Html::checkbox($name, $checked, [
                        'value' => $value,
                        'data-id' => $label,
                        'disabled' => $disable,
                        'label' => '<span class="custom-control-label">' . $label . '</span>',
                        'labelOptions' => ['class' => 'custom-control custom-checkbox',],
                        'class' => 'custom-control-input myCheckBoxGroup',
                    ]);
                },
                'separator' => false, 'class' => 'custom-controls-stacked',
            ])->label(false);
        ?>

    </div>
    <div class="col-lg-6">
        <h3 class="card-title">Main Category</h3>
        <?php
        $session = Yii::$app->session;
        //->where(['status' => 'A'])->orderBy('city ASC')->groupBy(['city'])
        //->joinWith(['categoriesSaloon'])
        $dlist = \common\models\BusinessCategories::find()
                ->joinWith(['categoriesSaloon'])
                ->where(['=', 'shops_id', $session['shopID']])
                ->asArray()
                ->all();
        $d_area = ArrayHelper::map($dlist, 'id', 'name');
        echo $form->field($model, 'main_category')->dropDownList($d_area,
                ['prompt'=>'Select...'])->label(false);
        ?>
    </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
    $("#categoriesform-category :checkbox").change(function(e){
    if ($(this).prop('checked') == true) {
        $('<option value="'+ $(this).val()+ '">'+ $(this).data('id') + '</option>').appendTo("#categoriesform-main_category");
    }else {
        var optionval = $(this).val();
        $('#categoriesform-main_category > option[value=' + optionval + ']').remove();
    }
});
$("#categoriesform-main_category").change(function(){
        $(':checkbox').each(function () {
            $(this).prop('disabled', false);
        });
        var val = $(this).val();
        $('#categoriesform-category input:checkbox[value="'+val+'"]').prop('disabled', true);
            
        });
JS;
$this->registerJs($script);
?>
