<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */

$session = Yii::$app->session;
$clientID = $session['currentclientID'];
$shopID = $session['shopID'];
?>

<div class="client-form">
    
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-lg-12">
        <h3 class="card-title">Availability of Your Business </h3>
        <?php
            $count = 0;
            $checkvalue = 0;
            $item_lists = ['1' => 'Monday', '2' => 'Tuesday', '3' => 'Wednesday', '4' => 'Thursday', '5' => 'Friday', '6' => 'Saturday', '7' => 'Sunday'];
            $start = "00:00"; //you can write here 00:00:00 but not need to it
            $end = "23:55";
            $open_time = "";
            $tStart = strtotime($start);
            $tEnd = strtotime($end);
            $tNow = $tStart;
            while($tNow <= $tEnd){
                //echo '<option value="'.date("H:i:s",$tNow).'">'.date("H:i:s",$tNow).'</option>';
                $open_time[] = array('time_value' => date("H:i:s",$tNow),'time_label' => date("h:i A",$tNow));
                $tNow = strtotime('+10 minutes',$tNow);
            }
            $time_list = ArrayHelper::map($open_time, 'time_value', 'time_label');

        ?>
        
        <?php foreach ($item_lists as $index => $value) : ?>
        
        <?php
            $selctdd = \common\models\ShopHours::find()->where(['shop_id' => $shopID, 'day_of_week' => $index])->count();
            if($selctdd > 0) {
                $selctdd = \common\models\ShopHours::find()->where(['shop_id' => $shopID, 'day_of_week' => $index])->one();
                if(!empty($selctdd->open_time) && !empty($selctdd->close_time)) {
                    $checkvalue = 1;
                }else {
                    $checkvalue = 0;
                }
                $model->day_of_week[$index] = $checkvalue;
                $model->open_time[$index] = $selctdd->open_time;
                $model->close_time[$index] = $selctdd->close_time;
            }
        ?>
    <div class="row">

        <div class="col-sm-4">
            <?= $form->field($model, "day_of_week[$index]", [
                'template' => "<div class=\"form-group\" ><label class=\"custom-switch\">{input}<span class=\"custom-switch-indicator\"></span><span class=\"custom-switch-description\">$value</span></label>\n{error}</div>"
            ])->checkbox(['class' => 'custom-switch-input'],false)
            ?>
            
        </div>
        <div class="col-sm-4">

            <?php            
            echo $form->field($model, "open_time[$index]")->dropDownList($time_list, [
                'prompt' => '-- Open --',
            ])->label(false);
            ?>
        </div>
        <div class="col-sm-4">
            <?php            
            echo $form->field($model, "close_time[$index]")->dropDownList($time_list, [
                'prompt' => '-- Close --',
            ])->label(false);
            ?>
        </div>
    </div>
<?php endforeach; ?>

    </div>
    
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
    $("#categoriesform-category :checkbox").change(function(e){
    if ($(this).prop('checked') == true) {
        $('<option value="'+ $(this).val()+ '">'+ $(this).data('id') + '</option>').appendTo("#categoriesform-main_category");
    }else {
        var optionval = $(this).val();
        $('#categoriesform-main_category > option[value=' + optionval + ']').remove();
    }
});
$("#categoriesform-main_category").change(function(){
        $(':checkbox').each(function () {
            $(this).prop('disabled', false);
        });
        var val = $(this).val();
        $('#categoriesform-category input:checkbox[value="'+val+'"]').prop('disabled', true);
            
        });
JS;
$this->registerJs($script);
?>
