<?php
use backend\assets\DashboardAsset;

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\components\Header;
use app\components\SidebarMenu;
use app\components\ChatSidebar;
use common\widgets\Alert;
use yii\bootstrap\Modal;
$baseURL = Yii::$app->request->url;

DashboardAsset::register($this);
//AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Saloon saloon srilanka saloon" />
    <meta name="author" content="Shihan" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<?php $this->beginBody() ?>
    
    <div class="page-wrapper">
        <!-- start header -->
        <?= Header::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        <!-- end header -->
        
        <!-- start page container -->
        <div class="page-container">
            <!-- start sidebar menu -->
                <?= SidebarMenu::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
            <!-- end sidebar menu --> 
            
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?= $this->title ?></div>
                            </div>
                            
                                <?php
                                echo Breadcrumbs::widget([
                                    'tag' => 'ol',
                                    'homeLink' => ['label' => '<i class="fa fa-home"></i> Home', 'url' => ['/index']],
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                    'encodeLabels' => false,
                                    //'delimiter'=>' / ',
                                    'options'=> array('class'=>'breadcrumb page-breadcrumb pull-right'),
                                ]);
                                //<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i></li>
                                ?>

                        </div>
                    </div>
                    <?= $content ?>
                </div>
            </div>
            <!-- end page content -->
            
            <!-- start chat sidebar -->
                <?= ChatSidebar::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        
        <!-- end footer -->
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
