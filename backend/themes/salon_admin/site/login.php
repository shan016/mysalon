<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login new';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap-login100">
    <?php $form = ActiveForm::begin(['id' => 'sign_in','options' => [
                'class' => 'login100-form validate-form'
             ]]); ?>

    <p class="text-center"><img src="/themes/saloon_theme/img/logo.png" alt=""/></p>
        <span class="login100-form-title p-b-34 p-t-27">
            Log in
        </span>
            <?= $form->field($model, 'email', ['options' => [],
                'template' => "<div class=\"wrap-input100 validate-input\" data-validate=\"Enter username\">{input}<span class=\"focus-input100\" data-placeholder=\"&#xf207;\"></span>\n{error}</div>"
            ])->textInput(['placeholder' => 'Email', 'class' => 'input100'])->label(false)
            ?>
    
            <?= $form->field($model, 'password', ['options' => [],
                'template' => "<div class=\"wrap-input100 validate-input\" data-validate=\"Enter password\">{input}<span class=\"focus-input100\" data-placeholder=\"&#xf191;\"></span></div>{error}"
            ])->passwordInput(['placeholder' => 'Password', 'class' => 'input100'])->label(false)
            ?>
    
            <?= $form->field($model, 'rememberMe', [
                'template' => "<div class=\"contact100-form-checkbox\" >{input}<label class=\"label-checkbox100\" for=\"ckb1\">Remember me</label>\n{error}</div>"
            ])->checkbox(['class' => 'input-checkbox100'],false)
            ?>

        <div class="container-login100-form-btn">
            <?= Html::submitButton('Login', ['class' => 'login100-form-btn']) ?>
        </div>
        <div class="text-center p-t-60">
            <a class="txt1" href="/site/request-password-reset">Forgot Password?</a>
        </div>
    <?php ActiveForm::end(); ?>
</div>

            


