<?php
use backend\assets\pinlist\DatatableAsset;

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\components\Header;
use app\components\SidebarMenu;
use app\components\Footer;
use common\widgets\Alert;
use yii\bootstrap\Modal;
$baseURL = Yii::$app->request->url;

DatatableAsset::register($this);
//AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--<meta name="description" content="Saloon saloon srilanka saloon" />
    <meta name="author" content="Shihan" />!-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body class="app sidebar-mini">
<?php $this->beginBody() ?>
    <div id="global-loader"><img src="/images/loader.svg" class="loader-img floating" alt=""></div>
    <div class="page">
        <div class="page-main">
            <!-- Header -->
            <?= Header::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
            <!-- Sidebar menu-->
            <?= SidebarMenu::widget(['path' => Yii::$app->request->getPathInfo()]) ?>

            <div class="app-content  my-3 my-md-5">
                <div class="side-app">
                    <div class="page-header">
                        <h4 class="page-title"><?= $this->title ?></h4>
                        <?php
                                echo Breadcrumbs::widget([
                                    'tag' => 'ol',
                                    //'homeLink' => ['label' => 'Home', 'url' => ['/index']],
                                    'activeItemTemplate' => "<li class='breadcrumb-item active'>{link}</li>",
                                    'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>",
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                    //'links' => '<li class="breadcrumb-item"><a href="{url}">{label}</a></li>',
                                    'encodeLabels' => true,
                                    //'delimiter'=>' / ',
                                    'options' => array('class' => 'breadcrumb')
                                ]);
                                //<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i></li>
                                ?>
                        
                    </div>
                    <div class="row">
                    <?= $content ?>
                    </div>
                    
                </div>
            </div>
        </div>

        <!--footer-->
        <?= Footer::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        <!-- End Footer-->
    </div>
    <!-- Back to top -->
    <a href="#top" id="back-to-top" ><i class="fa fa-rocket"></i></a>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
