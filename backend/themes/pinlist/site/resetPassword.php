<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

            <?php $form = ActiveForm::begin(['id' => 'sign_in']); ?>
            <div class="msg hide">Sign in to start your session</div>

                <?= $form->field($model, 'password', ['options' => [],
                    'template' => "<div class=\"input-group\"><span class='input-group-addon'><i class='material-icons'>person</i></span><div class='form-line'>{input}</div>\n{error}</div>"
                ])->passwordInput(['autofocus' => true, 'placeholder' => 'Password (max 12 characters)'])
                ?>

            <div class="row">

                <div class="col-xs-4">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-block bg-pink waves-effect', 'name' => 'login-button']) ?>
                </div>
            </div>
            <div class="row m-t-15 m-b--20">
                <div class="col-xs-6">
                </div>
                <div class="col-xs-6 align-right">
                    <a href="index">Back to Login</a>
                </div>
            </div>
            <?php ActiveForm::end(); ?>


