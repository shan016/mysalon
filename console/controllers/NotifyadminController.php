<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * 
 */
class NotifyadminController extends Controller {

    public function actionIndex() {
        $emails = "";

        $rows = \common\models\EmailBounced::find()
                ->where(['status' => 'P'])
                ->all();

        foreach ($rows as $row) {
            $emailBounced = \common\models\EmailBounced::findOne($row->id);

            $emails = $emails . "Email: " . $emailBounced->email . "<BR>";

            $user = \common\models\User::findByEmail($emailBounced->email);
            if ($user){
                $user->email_status = "HB";
                $user->save(false);
                $emailBounced->status = "C";
                $emailBounced->save(false);
            } else {
                $emailBounced->status = "X";
                $emailBounced->save(false);
            }
            
        }

        $emailSubject = "Email bounced";
        $emailBody = "Users with emails below were bounced.<BR><BR>" . $emails;

        Yii::$app->mailer->compose()
                ->setTo(Yii::$app->params['adminIresidenzEmail'])
                ->setFrom([\Yii::$app->params['posterMasterEmail'] => \Yii::$app->name])
                ->setReplyTo(\Yii::$app->params['replyTo'])
                ->setSubject($emailSubject)
                ->setHtmlBody($emailBody)
                ->send();
    }

}
