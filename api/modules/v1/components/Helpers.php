<?php

namespace api\modules\v1\components;
use Yii;
use mPDF;
use kartik\mpdf\Pdf;
use yii\helpers\Html;
use yii\base\Component;
use yii\base\Exception;

class Helpers
{
    

    public static function createPath($path) {
        if (is_dir($path))
            return true;
        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1);
        $return = Helpers::createPath($prev_path);
        return ($return && is_writable($prev_path)) ? mkdir($path) : false;
    }
    //--------------------------- Account 1 ------------------------------
    public function AvailablePointAcc1(){
        $clientID = Yii::$app->user->identity->client_id;
        $userid = Yii::$app->user->id;
        
        $expired = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'E'])
        ->sum('points');
        $expired = str_replace('-', '', $expired);
        if(empty($expired)){
            $expired = 0; 
         }

        $totaladd = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'A'])
        ->sum('points');
        
        if(empty($totaladd)){
            $totaladd = 0; 
        }
         
        $totalminus = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'V']) 
        ->sum('points');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
         
        
        $adjustment = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'B'])
        ->sum('points');
        
        $totaladjustment = str_replace('-', '', $adjustment);
        if(empty($totalminus)){
            $totaladjustment = 0; 
         }
         
        $balance = $totaladd - $totalminus - $expired;
        $totalbalance = $balance + $totaladjustment;
        
        //return $totalbalance;
        $text = 'Available Balance';
        if($totalbalance > 0){
            $totalbalance = $totalbalance;
        }else {
            $totalbalance = 0;
        }
        
        return [
            'message' => $text,
            'account1point' => $totalbalance
        ];
    }
    
    public function GoingtoExpireAcc1() {
        $tot = self::PointsExpireAcc1() - self::CurrentYearRedeemedAcc1();
        if($tot > 0) {
            $tot = $tot;
        }else {
            $tot = 0;
        }
        
        return $tot;
        
    }
    
    private static function PointsExpireAcc1() 
    {
        $clientID = Yii::$app->user->identity->client_id;
        $userid = Yii::$app->user->id;
        
        $total = 0;
        $pastcycle = \common\models\PointCycle::find()->where(['status' => 'I'])->orderBy(['point_cycle_name' => SORT_DESC])->one();

        $awarded = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'A'])
        ->andWhere(['<=', 'date_added', $pastcycle->awarded_end])         
        ->sum('points');
        
        $voide = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'V'])
        ->andWhere(['<=', 'date_added', $pastcycle->redemption_end])         
        ->sum('points');
        $voide = str_replace('-', '', $voide);
        
        $expired = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'E'])
        ->andWhere(['<=', 'date_added', $pastcycle->redemption_end])         
        ->sum('points');
        $expired = str_replace('-', '', $expired);
        
        $badjust = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'B'])
        ->andWhere(['<=', 'date_added', $pastcycle->redemption_end])         
        ->sum('points');
        $badjust = str_replace('-', '', $badjust);
        
        $total = $awarded - $voide - $expired + $badjust;

        return $total;
    }
    
    private static function CurrentYearRedeemedAcc1()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $userid = Yii::$app->user->id;
        
        $total = 0;
        $currentcycle = \common\models\PointCycle::find()->where(['status' => 'A'])->one();

       
        $currentvoide = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', $currentcycle->redemption_start])
        ->andWhere(['<=', 'date_added', $currentcycle->redemption_end])         
        ->sum('points');
        $currentvoide = str_replace('-', '', $currentvoide);
        
        $currentexpired = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'E'])
        ->andWhere(['>=', 'date_added', $currentcycle->redemption_start])
        ->andWhere(['<=', 'date_added', $currentcycle->redemption_end])         
        ->sum('points');
        $currentexpired = str_replace('-', '', $currentexpired);
        
        $currentadjust = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'B'])
        ->andWhere(['<=', 'date_added', $currentcycle->redemption_end])         
        ->sum('points');
        $currentadjust = str_replace('-', '', $currentadjust);

        $currentpoint = $currentvoide + $currentexpired;
        
        $total = $currentpoint;
        
        return $total;
    }

 
    //--------------------------- Account 2 ------------------------------
    public function AvailablePointAcc2(){
        $clientID = Yii::$app->user->identity->client_id;
        $userid = Yii::$app->user->id;

        $expired = \common\models\VIPCustomerAccount2::find()
                ->where(['clientID' => $clientID, 'customer_id' => $userid, 'bb_type' => 'E'])
                ->sum('points_in');
        $expired = str_replace('-', '', $expired);
        if (empty($expired)) {
            $expired = 0;
        }

        $totaladd = \common\models\VIPCustomerAccount2::find()
                ->where(['clientID' => $clientID, 'customer_id' => $userid, 'bb_type' => 'A'])
                ->sum('points_in');

        if (empty($totaladd)) {
            $totaladd = 0;
        }

        $totalminus = \common\models\VIPCustomerAccount2::find()
                ->where(['clientID' => $clientID, 'customer_id' => $userid, 'bb_type' => 'V'])
                ->sum('points_in');
        $totalminus = str_replace('-', '', $totalminus);
        if (empty($totalminus)) {
            $totalminus = 0;
        }

        $balance = $totaladd - $totalminus - $expired;
        $totalbalance = $balance;
        
        $text = 'Available Balance';
        if($totalbalance > 0){
            $totalbalance = $totalbalance;
        }else {
            $totalbalance = 0;
        }
        
        return [
            'message' => $text,
            'account2point' => $totalbalance
        ];
    }

    public function GoingtoExpireAcc2() {
        $tot = self::PointsExpireAcc2() - self::CurrentYearRedeemedAcc2();
        if($tot > 0) {
            $tot = $tot;
        }else {
            $tot = 0;
        }
        
        return $tot;
        
    }
    
    private static function PointsExpireAcc2() 
    {
        $clientID = Yii::$app->user->identity->client_id;
        $userid = Yii::$app->user->id;
        
        $total = 0;
        $pastcycle = \common\models\PointCycleAcc2::find()->where(['status' => 'I'])->orderBy(['point_cycle_name' => SORT_DESC])->one();

        $awarded = \common\models\VIPCustomerAccount2::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'A'])
        ->andWhere(['<=', 'date_added', $pastcycle->awarded_end])         
        ->sum('points_in');
        
        $voide = \common\models\VIPCustomerAccount2::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'V'])
        ->andWhere(['<=', 'date_added', $pastcycle->redemption_end])         
        ->sum('points_in');
        $voide = str_replace('-', '', $voide);
        
        $expired = \common\models\VIPCustomerAccount2::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'E'])
        ->andWhere(['<=', 'date_added', $pastcycle->redemption_end])         
        ->sum('points_in');
        $expired = str_replace('-', '', $expired);
        
        /*$badjust = \common\models\VIPCustomerAccount2::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'B'])
        ->andWhere(['<=', 'date_added', $pastcycle->redemption_end])         
        ->sum('points');
        $badjust = str_replace('-', '', $badjust);*/
        
        $total = $awarded - $voide - $expired;

        return $total;
    }
    
    private static function CurrentYearRedeemedAcc2()
    {
        $clientID = Yii::$app->user->identity->client_id;
        $userid = Yii::$app->user->id;
        
        $total = 0;
        $currentcycle = \common\models\PointCycleAcc2::find()->where(['status' => 'A'])->one();

       
        $currentvoide = \common\models\VIPCustomerAccount2::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'V'])
        ->andWhere(['>=', 'date_added', $currentcycle->redemption_start])
        ->andWhere(['<=', 'date_added', $currentcycle->redemption_end])         
        ->sum('points_in');
        $currentvoide = str_replace('-', '', $currentvoide);
        
        $currentexpired = \common\models\VIPCustomerAccount2::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'E'])
        ->andWhere(['>=', 'date_added', $currentcycle->redemption_start])
        ->andWhere(['<=', 'date_added', $currentcycle->redemption_end])         
        ->sum('points_in');
        $currentexpired = str_replace('-', '', $currentexpired);
        
        /*$currentadjust = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>$userid, 'bb_type' => 'B'])
        ->andWhere(['<=', 'date_added', $currentcycle->redemption_end])         
        ->sum('points');
        $currentadjust = str_replace('-', '', $currentadjust);*/

        $currentpoint = $currentvoide + $currentexpired;
        
        $total = $currentpoint;
        
        return $total;
    }

    public static function sendEmailSignup($userId,$data = null, $clientID) {
        //$emailBody = 'This test Email';
        $client = \common\models\Client::findOne($clientID);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $admin_url = $client->admin_domain;
        $client_name = $client->clientAddress->name;
        $client_email = $client->clientAddress->email_address;
        $client_programme_title = $client->programme_title;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['clientID' => $clientID, 'code' => 'EV1'])
                ->one();

        $emailSubject = $emailTemplate->subject;

        $profile = \common\models\VIPCustomer::find()->where(['userID' => $userId])->one();
        $name = $profile->full_name;
        $email = $profile->user->email;
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        $verification_email_key = isset($data2['verification_email_key']) ? $data2['verification_email_key'] : null;
        
        $emailBody = $emailTemplate->template;
        
        $emailSubject = str_replace('{name}', $name, $emailSubject);
        
        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        $emailBody = str_replace('{store_name}', $client_name, $emailBody);
        $emailBody = str_replace('{client_name}', $client_name, $emailBody);
        $emailBody = str_replace('{programme_title}', $client_programme_title, $emailBody);
        $emailBody = str_replace('{verification_email_key}', $verification_email_key, $emailBody);
        
        $emailBody = str_replace('/upload', $siteUrl . "/upload", $emailBody);

        if(!empty($client->email)){
            $clientemail = $client->email;
            Yii::$app->mailer->compose()
            ->setTo($email)
            ->setBcc($clientemail)
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }else {
            Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }
        
    }

    public static function addtolog($status = null, $message = null, $uID = 0, $clientID = 0, $version = 0)
    {
    	\api\modules\v1\models\ActionLog::addtolog($status, $message, $uID, $clientID, $version);
    } 

}
