<?php
namespace api\modules\v1\controllers;

use yii\rest\Controller;
use Yii;

class ListsController extends Controller
{
    /* public $modelClass = '\common\models\User'; */
    public $modelClass = '\common\models\Salutation';
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['post'],
            ],
        ];
        return $behaviors;
    }


    /**
    * @api {get} lists/business-categories Business Categories
    * @apiGroup Country
    * @apiSuccess {Object[]} BusinessCategories
    * @apiSuccessExample {json} Success
    *   HTTP/1.1 200 OK
    *   {
    *       "id": 1,
    *       "name": "Barbershop",
    *   }
    * @apiErrorExample {json} Failure
    *   HTTP/1.1 404 Not Found
    *   {
    *       "name": "Not Found",
    *       "message": "Object not foud",
    *       "code": 0,
    *       "status": 404,
    *       "type": "yii\\web\\NotFoundHttpException"
    *   }
    */
    public function actionBusinessCategories()
    {
        $categories = \common\models\BusinessCategories::find()
            ->select(['id','name','categorie_image'])
            ->where(['status' => 'A'])
            ->orderBy([
                'name' => SORT_ASC,
            ])->all();
        return $categories;
    }
}