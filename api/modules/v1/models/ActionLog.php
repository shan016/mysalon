<?php

namespace api\modules\v1\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "actionlog".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $user_id
 * @property integer $client_id
 * @property string $user_remote
 * @property string $time
 * @property string $action
 * @property string $category
 * @property string $status
 * @property string $message
 * @property string $user_agent
 */
class ActionLog extends \yii\db\ActiveRecord
{
    const LOG_STATUS_SUCCESS = 'success';
    const LOG_STATUS_INFO = 'info';
    const LOG_STATUS_WARNING = 'warning';
    const LOG_STATUS_ERROR = 'error';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'actionlog';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'time',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
    * Adds a message to ActionLog model
    *
    * @param string $status The log status information
    * @param mixed $message The log message
    * @param int $uID The user id
    */
    public static function addtolog($status = null, $message = null, $uID = 0, $clientID = 0 , $version = 0)
    {              
            $model = Yii::createObject(__CLASS__);
            $model->user_id = ((int)$uID !== 0) ? (int)$uID : (int)$model->getUserID();
            $model->client_id = $clientID !=null? $clientID: 0;
            $model->user_remote = $_SERVER['REMOTE_ADDR'];
            $model->action = Yii::$app->requestedAction->id;
            
            $category = Yii::$app->requestedAction->controller->id.'|api-v1';
            if($version >0) 
            	$category .= '|app-v'.$version;
            $model->category = $category;
            
            $model->status = $status;
            $model->message = ($message !== null) ? serialize($message) : null;
            $model->user_agent = Yii::$app->request->userAgent;
            return $model->save();
        
    }
    /**
    * Get the current user ID
    *
    * @return int The user ID
    */

    public static function getUserID()
    {
        $user = Yii::$app->getUser();
        return $user && !$user->getIsGuest() ? $user->getId() : 0;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'user_id' => 'User ID',
            'client_id' => 'Client ID',
            'user_remote' => 'User Remote',
            'time' => 'Time',
            'action' => 'Action',
            'category' => 'Category',
            'status' => 'Status',
            'message' => 'Message',
            'user_agent' => 'User Agent',
        ];
    }
}
