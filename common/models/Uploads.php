<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "uploads".
 *
 * @property int $upload_id
 * @property string|null $gallery_type S-Shop W-Work
 * @property string $ref
 * @property string $file_name
 * @property string $real_filename
 * @property string|null $create_date
 */
class Uploads extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'uploads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ref', 'file_name', 'real_filename'], 'required'],
            [['ref','create_date'], 'safe'],
            [['gallery_type'], 'string', 'max' => 1],
            [['file_name', 'real_filename'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'upload_id' => 'Upload ID',
            'gallery_type' => 'Gallery Type',
            'ref' => 'Ref',
            'file_name' => 'File Name',
            'real_filename' => 'Real Filename',
            'create_date' => 'Create Date',
        ];
    }
}
