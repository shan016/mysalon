<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "client".
 *
 * @property int $client_id
 * @property string $company_name
 * @property string $company_email
 * @property string $company_tel
 * @property string $company_fax
 * @property string $shop_link
 * @property string $short_business_description
 * @property string $client_status P=Pending A=Active I=Inactive  X=Delete
 *
 * @property User[] $users
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_name','company_email'], 'required'],
            [['shop_link', 'short_business_description'], 'string'],
            [['company_name'], 'string', 'max' => 300],
            [['company_email'], 'string', 'max' => 255],
            [['company_tel', 'company_fax'], 'string', 'max' => 100],
            [['client_status'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Client ID',
            'company_name' => 'Company Name',
            'company_email' => 'Email',
            'company_tel' => 'Tel',
            'company_fax' => 'Fax',
            'shop_link' => 'Website',
            'short_business_description' => 'About Company',
            'client_status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['clientID' => 'client_id']);
    }
}
