<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "business_categories".
 *
 * @property int $id
 * @property string $name
 * @property string $status
 */
class BusinessCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'business_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 200],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }
    
    public function getCategoriesSaloon()
    {
        return $this->hasOne(BusinessCategoriesSaloon::className(), ['business_categories_id' => 'id']);
    }
}
