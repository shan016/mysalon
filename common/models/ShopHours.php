<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shop_hours".
 *
 * @property int $id
 * @property int $shop_id
 * @property int $day_of_week
 * @property string|null $open_time
 * @property string|null $close_time
 */
class ShopHours extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shop_hours';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shop_id', 'day_of_week'], 'required'],
            [['shop_id', 'day_of_week'], 'integer'],
            [['open_time', 'close_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shop_id' => 'Shop ID',
            'day_of_week' => 'Day Of Week',
            'open_time' => 'Open Time',
            'close_time' => 'Close Time',
        ];
    }
}
