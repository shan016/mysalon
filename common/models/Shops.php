<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shops".
 *
 * @property int $shops_id
 * @property int $client_id
 * @property string $name_of_salon
 * @property string $address
 * @property int $postcode
 * @property string $city
 * @property string $province
 * @property string $district
 * @property double $lat
 * @property double $lng
 * @property string $contact_no
 * @property string $profile_public N=No Y=Yes
 * @property int $main_shop
 */
class Shops extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shops';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'name_of_salon', 'address', 'postcode', 'city', 'district', 'description'], 'required'],
            [['client_id', 'postcode', 'main_shop'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['name_of_salon'], 'string', 'max' => 200],
            [['contact_no'], 'string', 'max' => 12],
            [['description'], 'string', 'max' => 600],
            [['address', 'city', 'province', 'district'], 'string', 'max' => 100],
            [['profile_public'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'shops_id' => 'Shops ID',
            'client_id' => 'Client ID',
            'name_of_salon' => 'Name Of Salon',
            'address' => 'Address',
            'description' => 'Description',
            'postcode' => 'Postcode',
            'city' => 'City',
            'province' => 'Province',
            'district' => 'District',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'contact_no' => 'Contact No',
            'profile_public' => 'Profile Public',
            'main_shop' => 'Main Shop',
        ];
    }
}
