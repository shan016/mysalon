<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class ShopHoursForm extends Model
{
    public $day_of_week;
    public $open_time;
    public $close_time;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['day_of_week', 'open_time', 'close_time'], 'safe'],
            //['excel', 'file'],
            //[['excel'], 'file', 'extensions' => 'xls, xlsx'],
        ];
    }

    
}
