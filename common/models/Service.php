<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property int $service_id
 * @property int $shop_id
 * @property string $service_name
 * @property int|null $service_categories_id
 * @property string $duration
 * @property string|null $price_type
 * @property float $price
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $s_hour;
    public $s_minute;
    
    public static function tableName()
    {
        return 'service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shop_id', 'service_name', 'duration', 'price'], 'required'],
            [['shop_id', 'service_categories_id'], 'integer'],
            [['duration','s_hour','s_minute'], 'safe'],
            [['price'], 'number'],
            [['service_name'], 'string', 'max' => 100],
            [['price_type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'service_id' => 'Service ID',
            'shop_id' => 'Shop ID',
            'service_name' => 'Service Name',
            'service_categories_id' => 'Service Categories',
            'duration' => 'Duration',
            'price_type' => 'Price Type',
            'price' => 'Price',
            's_hour' => 'Hour',
            's_minute' => 'Minute'
            
        ];
    }
}
