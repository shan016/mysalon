<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service_categories".
 *
 * @property int $service_categories_id
 * @property int $shop_id
 * @property string $category_name
 */
class ServiceCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shop_id', 'category_name'], 'required'],
            [['shop_id'], 'integer'],
            [['category_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'service_categories_id' => 'Service Categories ID',
            'shop_id' => 'Shop ID',
            'category_name' => 'Category Name',
        ];
    }
}
