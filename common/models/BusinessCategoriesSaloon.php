<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "business_categories_saloon".
 *
 * @property int $id
 * @property int $business_categories_id
 * @property int $shops_id
 * @property int $default_id
 */
class BusinessCategoriesSaloon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'business_categories_saloon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business_categories_id', 'shops_id', 'default_id'], 'required'],
            [['business_categories_id', 'shops_id', 'default_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business_categories_id' => 'Business Categories ID',
            'shops_id' => 'Shops ID',
            'default_id' => 'Default ID',
        ];
    }
}
